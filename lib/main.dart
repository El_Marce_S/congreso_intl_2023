import 'package:flutter/material.dart';
import 'package:timezone/data/latest.dart' as tz;

import 'core/navigation/route_navigator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  tz.initializeTimeZones();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: router,
      title: 'CID2023',
    );
  }
}