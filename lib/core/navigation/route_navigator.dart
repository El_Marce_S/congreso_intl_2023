import 'package:cid2023/features/home_page/presenter/page/large_layout/large_layout_home_page.dart';
import 'package:cid2023/features/home_page/presenter/page/responsive_home_page.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/responsive_landing_page.dart';
import 'package:cid2023/features/profile/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

final GoRouter router = GoRouter(
  initialLocation: '/home',
  routes: [
    GoRoute(
      path: '/landing',
      builder: (BuildContext context, GoRouterState state) =>
          const ResponsiveLandingPage(),
    ),
    ShellRoute(
      builder: (_, __, child) {
        return LargeLayoutHome(
          contentWidget: child,
        );
      },
      routes: [
        GoRoute(
          path: '/home',
          builder: (BuildContext context, GoRouterState state) {
            return const ResponsiveHomePage();
          },
        ),
        GoRoute(
          name: 'speaker-profile',
          path: '/speaker/:speakerId',
          builder: (BuildContext context, GoRouterState state) {
            final speakerId = state.pathParameters['speakerId'] ?? '';
            return ProfilePage(speakerId: speakerId);
          },
        ),
      ],
    ),
  ],
);
