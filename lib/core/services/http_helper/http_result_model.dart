class HttpResult {
  final String data;
  final int statusCode;
  final String? errorMessage;

  HttpResult(this.data, this.statusCode, {this.errorMessage});

  factory HttpResult.ok(String data) {
    return HttpResult(data, 200);
  }

  factory HttpResult.err(String errorMessage, [int statusCode = 500]) {
    return HttpResult('', statusCode, errorMessage: errorMessage);
  }

  T unwrap<T>() {
    return data as T;
  }

  T when<T>({
    required T Function(String data) ok,
    required T Function(String errorMessage, int statusCode) err,
  }) {
    if (statusCode == 200) {
      return ok(data);
    } else {
      return err(errorMessage!, statusCode);
    }
  }
}
