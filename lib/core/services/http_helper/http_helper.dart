import 'dart:convert';

import 'package:cid2023/core/services/http_helper/http_interface.dart';
import 'package:cid2023/core/services/http_helper/http_result_model.dart';
import 'package:http/http.dart' as http;

class HttpHelper extends HttpInteface {
  final String baseUrl = 'https://api.congreso-dolor-2023-bolivia.org';

  // final String baseUrl = 'http://localhost:3000';

  HttpHelper();

  @override
  Future<HttpResult> get(String endpoint) async {
    final url = _buildUrl(endpoint);
    final response = await http.get(Uri.parse(url));
    return _processResponse(response);
  }

  @override
  Future<HttpResult> post(String endpoint,
      {dynamic body, String contentType = 'application/json'}) async {
    return _sendRequest(endpoint,
        body: body, contentType: contentType, method: http.post);
  }

  @override
  Future<HttpResult> put(String endpoint,
      {dynamic body, String contentType = 'application/json'}) async {
    return _sendRequest(endpoint,
        body: body, contentType: contentType, method: http.put);
  }

  Future<HttpResult> delete(String endpoint,
      {String contentType = 'application/json'}) async {
    final url = _buildUrl(endpoint);
    final response = await http.delete(
      Uri.parse(url),
      headers: {'Content-Type': contentType},
    );
    return _processResponse(response);
  }

  Future<HttpResult> _sendRequest(String endpoint,
      {dynamic body,
      String contentType = 'application/json',
      Future<http.Response> Function(Uri url,
              {Map<String, String>? headers, dynamic body})?
          method}) async {
    final url = _buildUrl(endpoint);
    final headers = {'Content-Type': contentType};
    dynamic processedBody;

    if (contentType == 'application/json' && body != null) {
      processedBody = json.encode(body);
    } else {
      processedBody = body;
    }

    final response =
        await method!(Uri.parse(url), headers: headers, body: processedBody);
    return _processResponse(response);
  }

  HttpResult _processResponse(http.Response response) {
    final responseBody = response.body;
    if (response.statusCode == 200) {
      return HttpResult.ok(responseBody);
    } else {
      final errorCode = response.statusCode;
      final errorMessage = response.reasonPhrase ?? 'Unknown error';
      return HttpResult.err(errorMessage, errorCode);
    }
  }

  String _buildUrl(String endpoint) {
    final url = '$baseUrl$endpoint';
      ('URL completa: $url');
    return url;
  }
}
