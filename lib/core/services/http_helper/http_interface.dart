import 'package:cid2023/core/services/http_helper/http_result_model.dart';

abstract class HttpInteface {
  Future<HttpResult> get(String endpoint);

  Future<HttpResult> post(String endpoint, {dynamic body});

  Future<HttpResult> put(String endpoint, {dynamic body});
}
