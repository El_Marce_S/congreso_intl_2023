final Map<String, String> countryNameToCode = {
  'España': 'es',
  'Argentina': 'ar',
  'Panamá': 'pa',
  'México': 'mx', // Ya estaba en la lista, así que no lo he duplicado
  'Brasil': 'br',
  'Canadá': 'ca',
  'Chile': 'cl',
  'Paraguay': 'py', // Ya estaba en la lista, así que no lo he duplicado
  'USA': 'us',
  'República Dominicana': 'do',
  'Uruguay': 'uy',
};

String getCountryCode(String countryName) {
  return countryNameToCode[countryName] ?? 'BO';
}
