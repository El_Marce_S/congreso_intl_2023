part of 'landing_page_bloc.dart';

abstract class LandingPageState extends Equatable {
  const LandingPageState();
}

class LandingPageInitialState extends LandingPageState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LandingPageState {
  @override
  List<Object> get props => [];
}

class SavedDataState extends LandingPageState {
  @override
  List<Object> get props => [];
}

class ErrorState extends LandingPageState {
  const ErrorState();

  @override
  List<Object> get props => [];
}
