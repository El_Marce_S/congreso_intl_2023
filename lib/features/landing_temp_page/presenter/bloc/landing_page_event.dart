part of 'landing_page_bloc.dart';

abstract class LandingPageEvent extends Equatable {
  const LandingPageEvent();
}

class SendFormDataEvent extends LandingPageEvent {
  const SendFormDataEvent(
      {required this.name,
      required this.lastName,
      required this.phone,
      required this.query,
      required this.email,
      required this.selectedCountry,
      required this.countryCode,
      required this.country});

  final String name;
  final String lastName;
  final String phone;
  final String query;
  final String email;
  final String selectedCountry;
  final String countryCode;
  final String country;

  @override
  List<Object?> get props => [
        name,
        lastName,
        phone,
        query,
        email,
        selectedCountry,
        countryCode,
        country
      ];
}
