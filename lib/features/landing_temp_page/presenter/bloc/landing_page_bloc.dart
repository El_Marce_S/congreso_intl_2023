import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cid2023/features/landing_temp_page/data/models/contact_model.dart';
import 'package:cid2023/features/landing_temp_page/domain/repository/landing_repository.dart';
import 'package:equatable/equatable.dart';

part 'landing_page_event.dart';

part 'landing_page_state.dart';

class LandingPageBloc extends Bloc<LandingPageEvent, LandingPageState> {
  LandingPageBloc({required this.landingRepository})
      : super(LandingPageInitialState()) {
    on<LandingPageEvent>((event, emit) {});
    on<SendFormDataEvent>(_sendFormDataEvent);
  }

  final LandingRepository landingRepository;

  Future<void> _sendFormDataEvent(
      SendFormDataEvent event, Emitter<LandingPageState> emit) async {
    emit(
      LoadingState(),
    );

    final contact = ContactModel(
      firstName: event.name.toString(),
      lastName: event.lastName.toString(),
      email: event.email.toString(),
      phone: event.phone.toString(),
      countryCode: event.countryCode,
      country: event.country,
      query: event.query.toString(),
      selectedCountry: event.selectedCountry,
    );
    final result = await landingRepository.postNewContact(contact: contact);

    result.when(
      ok: (data) {
        emit(
          SavedDataState(),
        );
      },
      err: (error) {
        emit(
          const ErrorState(),
        );
      },
    );
  }
}


