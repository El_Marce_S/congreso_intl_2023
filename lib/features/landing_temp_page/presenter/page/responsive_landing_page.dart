import 'package:cid2023/features/landing_temp_page/data/api/landing_api.dart';
import 'package:cid2023/features/landing_temp_page/presenter/bloc/landing_page_bloc.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/large_layout/large_layout.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/small_layout/small_layout.dart';
import 'package:cid2023/shared/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ResponsiveLandingPage extends StatelessWidget {
  const ResponsiveLandingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LandingPageBloc>(
      create: (context) => LandingPageBloc(landingRepository: LandingApi()),
      child: const _Page(),
    );
  }
}

class _Page extends StatelessWidget {
  const _Page({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<LandingPageBloc, LandingPageState>(
      listener: (context, state) {
        if (state is LoadingState) {
          LoadingDialog(context);
          return;
        }
        if (state is SavedDataState) {
          Navigator.of(context).pop();
          return;
        }
      },
      child: const Scaffold(
        body: _Body(),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        if (mediaQuery.size.width > 1200) {
          return const LargeLayoutLandingPage();
        } else {
          return const SmallLayoutLandginPage();
        }
      },
    );
  }
}
