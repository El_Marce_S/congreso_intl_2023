import 'package:flutter/material.dart';

class LargeLandingHeader extends StatelessWidget {
  const LargeLandingHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isSmallScreen = MediaQuery.of(context).size.width < 900;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Text(
        'Estamos en la recta final. Pronto podrás disfrutar de nuestro sitio web completo! Si tienes alguna pregunta, Contáctanos.',
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'Typograph',
          fontWeight: FontWeight.w500,
          fontSize: isSmallScreen ? 30 : 50,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
