import 'dart:ui';

import 'package:cid2023/features/landing_temp_page/presenter/page/large_layout/widgets/large_home_header.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/banner_area.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/countdown_row.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/download_btn.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/form_area.dart';
import 'package:flutter/material.dart';

class LargeLayoutLandingPage extends StatelessWidget {
  const LargeLayoutLandingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image.network(
              'https://images.unsplash.com/photo-1551076805-e1869033e561?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1332&q=80',
              fit: BoxFit.cover,
            ),
          ),
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
              child: Container(
                color: Colors.black.withOpacity(0.5),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.10,
                    ),
                    child: const LargeLandingHeader(),
                  ),
                  const CountDownRow(),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FormArea(),
                      const SizedBox(height: 20),
                      const DownloadBtn(),
                      const SizedBox(height: 20),
                      const BannerArea(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
