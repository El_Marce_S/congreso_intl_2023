import 'package:flutter/material.dart';

class BannerArea extends StatelessWidget {
  const BannerArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 450, child: Image.asset('assets/images/afiche001.jpg'));
  }
}
