import 'dart:html' as html;

import 'package:flutter/material.dart';

class DownloadBtn extends StatelessWidget {
  const DownloadBtn({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.2, horizontal: 20),
      child: ElevatedButton(
        onPressed: _launchURL,
        style: ElevatedButton.styleFrom(
          primary: Colors.teal,
        ),
        child: const Padding(
          padding: EdgeInsets.all(8.0),
          child: Text('Descarga el\nprograma preliminar',style: TextStyle(fontSize:20,fontWeight: FontWeight.bold,),
              textAlign: TextAlign.center, maxLines: 3,),
        ),
      ),
    );
  }
}

void _launchURL() {
  final url =
      'https://drive.google.com/file/d/1H25s77iECc8CCvMH_ze_1BzRnnx7QNlT/view?usp=sharing';

  html.window.open(url, '_blank');
}
