import 'dart:async';

import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart' as tz;

class CountDownRow extends StatefulWidget {
  const CountDownRow({Key? key}) : super(key: key);

  @override
  _CountDownRowState createState() => _CountDownRowState();
}

class _CountDownRowState extends State<CountDownRow> {
  Timer? _timer;
  final tz.TZDateTime _endDate =
      tz.TZDateTime(tz.getLocation('America/La_Paz'), 2023, 8, 21);
  Duration _timeRemaining = const Duration();

  @override
  void initState() {
    super.initState();
    _timeRemaining = _endDate.difference(
      tz.TZDateTime.now(
        tz.getLocation('America/La_Paz'),
      ),
    );
    _startCountdown();
  }

  void _startCountdown() {
    _timer = Timer.periodic(
      const Duration(seconds: 1),
      (timer) {
        if (_timeRemaining.isNegative) {
          timer.cancel();
        } else {
          setState(() {
            _timeRemaining = _endDate.difference(
                tz.TZDateTime.now(tz.getLocation('America/La_Paz')));
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int days = _timeRemaining.inDays;
    int hours = _timeRemaining.inHours.remainder(24);
    int minutes = _timeRemaining.inMinutes.remainder(60);
    int seconds = _timeRemaining.inSeconds.remainder(60);

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCard(days.toString().padLeft(2, '0'), "DÍAS"),
          _buildCard(hours.toString().padLeft(2, '0'), "HORAS"),
          _buildCard(minutes.toString().padLeft(2, '0'), "MINUTOS"),
          _buildCard(seconds.toString().padLeft(2, '0'), "SEGUNDOS"),
        ],
      ),
    );
  }

  Widget _buildCard(String text, String label) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(
              text,
              style: const TextStyle(fontSize: 30),
            ),
            Text(
              label,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
