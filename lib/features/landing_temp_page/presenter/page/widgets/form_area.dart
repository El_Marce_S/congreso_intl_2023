import 'package:cid2023/features/landing_temp_page/presenter/bloc/landing_page_bloc.dart';
import 'package:cid2023/shared/success_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FormArea extends StatefulWidget {
  @override
  _FormAreaState createState() => _FormAreaState();
}

class _FormAreaState extends State<FormArea> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, String> countries = {
    'Argentina (+54)': 'AR',
    'Australia (+61)': 'AU',
    'Belize (+501)': 'BZ',
    'Bolivia (+591)': 'BO',
    'Canada (+1)': 'CA',
    'Chile (+56)': 'CL',
    'Colombia (+57)': 'CO',
    'Costa Rica (+506)': 'CR',
    'Cuba (+53)': 'CU',
    'Dominican Republic (+1)': 'DO',
    'Ecuador (+593)': 'EC',
    'El Salvador (+503)': 'SV',
    'Gibraltar (+350)': 'GI',
    'Guatemala (+502)': 'GT',
    'Honduras (+504)': 'HN',
    'Ireland (+353)': 'IRE',
    'Jamaica (+1)': 'JM',
    'Mexico (+52)': 'MX',
    'New Zealand (+64)': 'NZ',
    'Nicaragua (+505)': 'NI',
    'Panama (+507)': 'PA',
    'Paraguay (+595)': 'PY',
    'Peru (+51)': 'PE',
    'Philippines (+63)': 'PH',
    'Puerto Rico (+1)': 'PR',
    'Spain (+34)': 'ES',
    'United Kingdom (+44)': 'UK',
    'United States (+1)': 'US',
    'Uruguay (+598)': 'UY',
    'Venezuela (+58)': 'VE',
  };

  String _selectedCountry = 'Bolivia (+591)';
  final _nameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _queryController = TextEditingController();

  String get countryCode => _selectedCountry.split('(')[1].replaceAll(')', '');

  String get country => countries[_selectedCountry] ?? '';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Card(
        child: SizedBox(
          width: 450,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Center(
                child: Column(
                  children: <Widget>[
                    const Text(
                      'Estamos en plena construcción de nuestro nuevo sitio y'
                      ' nos gustaría mantenerte al tanto de nuestros avances.'
                      ' Te invitamos a dejarnos tus datos de contacto o cualquier '
                      'consulta o pregunta que tengas. Te aseguramos que nos pondremos en contacto contigo a la brevedad posible.',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.teal,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    TextFormField(
                      controller: _nameController,
                      decoration: const InputDecoration(
                        labelText: 'Nombre',
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nombre es necesario.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _lastNameController,
                      decoration: const InputDecoration(
                        labelText: 'Apellido',
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Apellido es necesario.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _emailController,
                      decoration: const InputDecoration(
                        labelText: 'Email',
                      ),
                      validator: (value) {
                        if (!value!.contains('@')) {
                          return 'Correo no parece ser correcto, por favor verifique.';
                        }
                        return null;
                      },
                    ),
                    DropdownButtonFormField(
                      value: _selectedCountry,
                      decoration: const InputDecoration(
                        labelText: 'País',
                      ),
                      items: countries.keys.map((country) {
                        return DropdownMenuItem(
                          value: country,
                          child: Text(country),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _selectedCountry = value!;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor seleccione un país.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _phoneController,
                      decoration: const InputDecoration(
                        labelText: 'Teléfono',
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value!.isEmpty || int.tryParse(value) == null) {
                          return 'Solo se aceptan números.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _queryController,
                      decoration: const InputDecoration(
                        labelText: 'Consulta o pregunta',
                      ),
                      maxLength: 500,
                      maxLines: 5,
                    ),
                    ElevatedButton(
                      child: const Text('Enviar'),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text('Processing Data'),
                            ),
                          );
                          context.read<LandingPageBloc>().add(
                                SendFormDataEvent(
                                  name: _nameController.value.text,
                                  lastName: _lastNameController.value.text,
                                  phone: _phoneController.value.text,
                                  query: _queryController.value.text,
                                  email: _emailController.value.text,
                                  selectedCountry: _selectedCountry,
                                  countryCode: countryCode,
                                  country: country,
                                ),
                              );
                          _formKey.currentState!.reset();
                          SuccessDialog.showSuccessDialog(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
