import 'dart:ui';

import 'package:cid2023/features/landing_temp_page/presenter/page/large_layout/widgets/large_home_header.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/banner_area.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/download_btn.dart';
import 'package:cid2023/features/landing_temp_page/presenter/page/widgets/form_area.dart';
import 'package:flutter/material.dart';

class SmallLayoutLandginPage extends StatelessWidget {
  const SmallLayoutLandginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image.network(
              'https://images.unsplash.com/photo-1551076805-e1869033e561?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1332&q=80',
              fit: BoxFit.cover,
            ),
          ),
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
              child: Container(
                color: Colors.black.withOpacity(0.5),
              ),
            ),
          ),
          Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height * 0.05),
                    child: const LargeLandingHeader(),
                  ),
                  const BannerArea(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const DownloadBtn(),
                        FormArea(),
                        const SizedBox(height: 20),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
