import 'package:cid2023/features/landing_temp_page/domain/entity/contact_entity.dart';

class ContactModel extends ContactEntity {
  ContactModel({
    required String firstName,
    required String lastName,
    required String email,
    required String phone,
    required String countryCode,
    required String country,
    required String query,
    required String selectedCountry,
  }) : super(
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: phone,
          countryCode: countryCode,
          country: country,
          query: query,
    selectedCountry: selectedCountry,
        );

  factory ContactModel.fromJson(Map<String, dynamic> json) {
    return ContactModel(
      firstName: json['firstName'] ?? '',
      lastName: json['lastName'] ?? '',
      email: json['email'] ?? '',
      phone: json['phone'] ?? '',
      countryCode: json['countryCode'] ?? '',
      country: json['country'] ?? '',
      query: json['query'] ?? '',
      selectedCountry: json['selectedCountry'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': firstName,
      'lastName': lastName,
      'email': email,
      'phone': phone,
      'selectedCountry': selectedCountry,
      'countryCode': countryCode,
      'country': country,
      'query': query,
    };
  }
}
