import 'dart:convert';

import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/core/services/http_helper/http_help.dart';
import 'package:cid2023/features/landing_temp_page/data/models/contact_model.dart';
import 'package:cid2023/features/landing_temp_page/domain/repository/landing_repository.dart';
import 'package:oxidized/oxidized.dart';

class LandingApi extends LandingRepository {
  @override
  Future<Result<bool, Failure>> postNewContact(
      {required ContactModel contact}) async {
    final httpHelper = HttpHelper();
    try {
      const endpoint = '/landing/register-new-contact';
      final contactJson = json.encode(contact.toJson());
      final response = await httpHelper.post(endpoint, body: contactJson);

      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final saved = decodedData['saved'] as bool;
          return Result.ok(saved);
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
      return Result.err(ServerFailure(message: e.toString()));
    }
  }
}
