import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/features/landing_temp_page/data/models/contact_model.dart';
import 'package:oxidized/oxidized.dart';

abstract class LandingRepository {
  Future<Result<bool, Failure>> postNewContact({required ContactModel contact});
}
