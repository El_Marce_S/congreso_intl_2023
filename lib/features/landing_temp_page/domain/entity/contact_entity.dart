class ContactEntity {
  const ContactEntity({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phone,
    required this.countryCode,
    required this.country,
    required this.query,
    required this.selectedCountry,
  });

  final String firstName;
  final String lastName;
  final String email;
  final String phone;
  final String countryCode;
  final String country;
  final String query;
  final String selectedCountry;
}
