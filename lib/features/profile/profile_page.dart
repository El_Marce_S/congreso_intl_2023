import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key, required this.speakerId});

  final String speakerId;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(speakerId),
    );
  }
}
