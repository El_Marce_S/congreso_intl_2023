import 'dart:convert';

import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/core/services/http_helper/http_help.dart';
import 'package:cid2023/features/home_page/data/models/doctor_model.dart';
import 'package:cid2023/features/home_page/data/models/sponsor_model.dart';
import 'package:cid2023/features/home_page/domain/repositories/home_repository.dart';
import 'package:oxidized/oxidized.dart';

class HomeApi extends HomeRepository {
  @override
  Future<Result<List<DoctorModel>, Failure>> getAllSpeakers() async {
    final httpHelper = HttpHelper();
    try {
      const endpoint = '/home/get-all-speakers';
      final response = await httpHelper.get(endpoint);

      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final doctors = (decodedData as List)
              .map((doctor) => DoctorModel.fromJson(doctor))
              .toList();
          return Result.ok(doctors);
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
      return Result.err(ServerFailure(message: e.toString()));
    }
  }

  @override
  Future<Result<String, Failure>> newSummitEnrollment({
    required String name,
    required String lastName,
    required String email,
    required String country,
    required String phone,
  }) async {
    final httpHelper = HttpHelper();
    try {
      const endpoint = '/enrollments/summit-enroll';
      final response = await httpHelper.post(endpoint, body: {
        'name': name,
        'lastName': lastName,
        'email': email,
        'country': country,
        'phone': phone,
      });

      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final debtUrl = decodedData['debtUrl'] as String;
            (debtUrl);
          return Result.ok(debtUrl);
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
        (e);
      return Result.err(ServerFailure(message: e.toString()));
    }
  }

  @override
  Future<Result<List<SponsorModel>, Failure>> getAllSponsors() async {
    final httpHelper = HttpHelper();
    try {
      const endpoint = '/home/get-all-sponsors';
      final response = await httpHelper.get(endpoint);

      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final sponsors = (decodedData as List)
              .map((sponsor) => SponsorModel.fromJson(sponsor))
              .toList();
          return Result.ok(sponsors);
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
      return Result.err(ServerFailure(message: e.toString()));
    }
  }
}
