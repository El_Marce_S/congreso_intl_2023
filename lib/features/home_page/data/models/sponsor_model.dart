import 'package:cid2023/features/home_page/domain/entities/sponsor_entity.dart';

class SponsorModel extends SponsorEntity {
  SponsorModel({
    required String uuid,
    required String sponsorName,
    required String sponsorLevel,
    required String sponsorLink,
    required String sponsorLogo,
  }) : super(
    uuid: uuid,
    sponsorName: sponsorName,
    sponsorLevel: sponsorLevel,
    sponsorLink: sponsorLink,
    sponsorLogo: sponsorLogo,
  );

  factory SponsorModel.fromJson(Map<String, dynamic> json) {
    return SponsorModel(
      sponsorName: json['sponsorName'] ?? '',
      sponsorLevel: json['sponsorLevel'] ?? '',
      sponsorLink: json['sponsorLink'] ?? '',
      sponsorLogo: json['sponsorLogo'] ?? '',
      uuid: json['_id'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'sponsorName': sponsorName,
      'sponsorLevel': sponsorLevel,
      'sponsorLink': sponsorLink,
      'sponsorLogo': sponsorLogo,
    };
  }
}
