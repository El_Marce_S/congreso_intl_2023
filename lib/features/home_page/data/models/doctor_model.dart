import 'package:cid2023/features/home_page/domain/entities/doctor_entity.dart';

class DoctorModel extends DoctorEntity {
  DoctorModel({
    required String uuid,
    required String name,
    required String lastName,
    required String country,
    required String position,
    required String summary,
    required String cvExperience,
    required String imageUrl,
  }) : super(
          uuid: uuid,
          name: name,
          lastName: lastName,
          country: country,
          position: position,
          summary: summary,
          cvExperience: cvExperience,
          imageUrl: imageUrl,
        );

  factory DoctorModel.fromJson(Map<String, dynamic> json) {
    return DoctorModel(
      name: json['name'] ?? '',
      lastName: json['lastName'] ?? '',
      country: json['country'] ?? '',
      position: json['position'] ?? '',
      summary: json['summary'] ?? '',
      cvExperience: json['cvExperience'] ?? '',
      imageUrl: json['picture'] ?? '',
      uuid: json['_id'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'lastName': lastName,
      'country': country,
      'position': position,
      'summary': summary,
      'cvExperience': cvExperience,
      'imageUrl': imageUrl,
    };
  }
}
