import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/features/home_page/data/models/doctor_model.dart';
import 'package:cid2023/features/home_page/data/models/sponsor_model.dart';
import 'package:oxidized/oxidized.dart';

abstract class HomeRepository {
  Future<Result<List<DoctorModel>, Failure>> getAllSpeakers();

  Future<Result<String, Failure>> newSummitEnrollment({
    required String name,
    required String lastName,
    required String email,
    required String country,
    required String phone,
  });

  Future<Result<List<SponsorModel>, Failure>> getAllSponsors();
}
