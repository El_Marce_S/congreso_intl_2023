class DoctorEntity {
  const DoctorEntity({
    required this.uuid,
    required this.name,
    required this.lastName,
    required this.country,
    required this.position,
    required this.summary,
    required this.cvExperience,
    required this.imageUrl,
  });

  final String name;
  final String uuid;
  final String lastName;
  final String country;
  final String position;
  final String summary;
  final String cvExperience;
  final String imageUrl;
}
