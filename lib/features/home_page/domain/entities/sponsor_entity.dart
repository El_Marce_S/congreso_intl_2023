class SponsorEntity {
  const SponsorEntity({
    required this.uuid,
    required this.sponsorName,
    required this.sponsorLevel,
    required this.sponsorLink,
    required this.sponsorLogo,
  });

  final String uuid;
  final String sponsorName;
  final String sponsorLevel;
  final String sponsorLink;
  final String sponsorLogo;
}
