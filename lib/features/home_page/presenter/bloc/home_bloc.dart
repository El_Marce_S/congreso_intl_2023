import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cid2023/features/home_page/data/models/doctor_model.dart';
import 'package:cid2023/features/home_page/data/models/sponsor_model.dart';
import 'package:cid2023/features/home_page/domain/repositories/home_repository.dart';
import 'package:equatable/equatable.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc({required this.homeRepository}) : super(HomeInitial()) {
    on<HomeEvent>((event, emit) {});
    on<GetSpeakerDoctorsEvent>(_onGetSpeakerDoctorsEvent);
    on<GetSponsorsEvent>(_onGetSponsorsEvent);
    on<SummitEnrollEvent>(_onSummitEnroll);
  }

  final HomeRepository homeRepository;

  Future<void> _onGetSpeakerDoctorsEvent(
      GetSpeakerDoctorsEvent event, Emitter<HomeState> emit) async {
    emit(
      const GettingDoctorsState(),
    );
    final result = await homeRepository.getAllSpeakers();
    result.when(
      ok: (data) {
        emit(
          LoadedSpeakers(doctorList: data),
        );
      },
      err: (e) {
        emit(
          ErrorState(
            message: e.message,
          ),
        );
      },
    );
  }

  Future<void> _onSummitEnroll(
      SummitEnrollEvent event, Emitter<HomeState> emit) async {
    emit(
      const LoadingState(),
    );

    final createDebtResponse = await homeRepository.newSummitEnrollment(
      name: event.name,
      lastName: event.lastName,
      email: event.email,
      country: event.country,
      phone: event.phone,
    );

    createDebtResponse.when(
      ok: (data) {
        emit(
          LinkAwaitingState(debtLink: data),
        );
      },
      err: (e) {
        emit(
          ErrorState(
            message: e.message,
          ),
        );
      },
    );
  }

  Future<void> _onGetSponsorsEvent(
      GetSponsorsEvent event, Emitter<HomeState> emit) async {
    emit(
      const LoadingSponsorsState(),
    );
    final result = await homeRepository.getAllSponsors();
    result.when(
      ok: (data) {
        emit(
          LoadedSponsorsState(sponsorList: data),
        );
      },
      err: (e) {
        emit(
          ErrorState(
            message: e.message,
          ),
        );
      },
    );
  }
}
