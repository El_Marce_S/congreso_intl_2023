part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class GettingDoctorsState extends HomeState {
  const GettingDoctorsState();

  @override
  List<Object> get props => [];
}

class LoadedSpeakers extends HomeState {
  const LoadedSpeakers({required this.doctorList});

  final List<DoctorModel> doctorList;

  @override
  List<Object> get props => [doctorList];
}

class ErrorState extends HomeState {
  const ErrorState({required this.message});

  final String message;

  @override
  List<Object> get props => [message];
}

class LoadingState extends HomeState {
  const LoadingState();

  @override
  List<Object> get props => [];
}

class LinkAwaitingState extends HomeState {
  const LinkAwaitingState({required this.debtLink});

  final String debtLink;

  @override
  List<Object> get props => [];
}

class LoadingSponsorsState extends HomeState {
  const LoadingSponsorsState();

  @override
  List<Object> get props => [];
}

class LoadedSponsorsState extends HomeState {
  const LoadedSponsorsState({required this.sponsorList});

  final List<SponsorModel> sponsorList;

  @override
  List<Object> get props => [sponsorList];
}
