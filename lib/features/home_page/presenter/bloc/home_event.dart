part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class GetSpeakerDoctorsEvent extends HomeEvent {
  const GetSpeakerDoctorsEvent();

  @override
  List<Object?> get props => [];
}

class SummitEnrollEvent extends HomeEvent {
  const SummitEnrollEvent(
      {required this.name,
      required this.lastName,
      required this.email,
      required this.phone,
      required this.country});

  final String name;
  final String lastName;
  final String email;
  final String phone;
  final String country;

  @override
  List<Object?> get props => [];
}

class GetSponsorsEvent extends HomeEvent {
  const GetSponsorsEvent();

  @override
  List<Object?> get props => [];
}
