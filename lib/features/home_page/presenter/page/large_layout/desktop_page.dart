import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/contact_form.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/inital_cover_area.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/speakers_widget/speakers_area.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/summit_enrollment_area/summit_enrollment_area.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/summit_representative_area.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/top_bar.dart';
import 'package:flutter/material.dart';

class DesktopView extends StatelessWidget {
  final ScrollController _controller = ScrollController();

  DesktopView({Key? key}) : super(key: key);

  _scrollToIndex(int index) {
    double offset = 0;
    for (var i = 0; i < index; i++) {
      offset += _controller.position.maxScrollExtent / 6;
    }
    _controller.animateTo(
      offset,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBar(
          onButtonPressedList: [
            () => _scrollToIndex(0),
            () => _scrollToIndex(1),
            () => _scrollToIndex(2),
            () => _scrollToIndex(3),
            () => _scrollToIndex(4),
            () => _scrollToIndex(5),
          ],
        ),
        Expanded(
          child: ListView(
            controller: _controller,
            children: const [
              InitialCoverArea(),
              SummitRepresentativeArea(),
              SpeakersArea(),
              // SponsorsArea(),
              EnrollmentArea(),
              // WorkShopArea(),
              ContactForm(),
            ],
          ),
        ),
      ],
    );
  }
}
