import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

class PromoCountDown extends StatefulWidget {
  const PromoCountDown({super.key});

  @override
  _PromoCountDownState createState() => _PromoCountDownState();
}

class _PromoCountDownState extends State<PromoCountDown> {
  late int endTime;

  @override
  void initState() {
    super.initState();

    final now = DateTime.now();
    final promoEndDate = DateTime(now.year, 9, 15, 23, 59, 59);
    endTime = promoEndDate.millisecondsSinceEpoch;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            'Aproveche a inscribirse hasta el 15 de septiembre',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: CountdownTimer(
            endTime: endTime,
            widgetBuilder: (_, time) {
              if (time == null) {
                return const Text("¡Promoción ha terminado!");
              }
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildTimeBox('${time.days ?? 0}', 'DÍAS'),
                    const SizedBox(width: 8),
                    _buildTimeBox('${time.hours ?? 0}', 'HRS'),
                    const SizedBox(width: 8),
                    _buildTimeBox('${time.min ?? 0}', 'MIN'),
                    const SizedBox(width: 8),
                    _buildTimeBox('${time.sec ?? 0}', 'SEC'),
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }

  Widget _buildTimeBox(String time, String label) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Text(
            time,
            style: const TextStyle(color: Colors.white, fontSize: 24),
          ),
        ),
        const SizedBox(height: 4),
        Text(label, style: const TextStyle(color: Colors.white)),
      ],
    );
  }
}
