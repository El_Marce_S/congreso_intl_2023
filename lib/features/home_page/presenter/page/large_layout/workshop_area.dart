import 'package:flutter/material.dart';

import 'widgets/single_workshop_card.dart';

class WorkShopArea extends StatelessWidget {
  const WorkShopArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    int crossAxisCount;
    if (width >= 1200) {
      crossAxisCount = 3;
    } else if (width >= 800) {
      crossAxisCount = 2;
    } else {
      crossAxisCount = 1;
    }

    var itemWidth = width / crossAxisCount;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(100),
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Wrap(
                direction: Axis.horizontal,
                spacing: 10.0,
                runSpacing: 10.0,
                children: List<Widget>.generate(
                  10,
                  (int index) {
                    return Container(
                      width: itemWidth,
                      padding: const EdgeInsets.all(20),
                      child: SingleWorkshopCard(index: index),
                    );
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
