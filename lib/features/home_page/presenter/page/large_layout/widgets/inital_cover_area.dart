import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class InitialCoverArea extends StatelessWidget {
  const InitialCoverArea({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    double commonWidth;
    if (screenWidth > 1200) {
      commonWidth = 400;
    } else if (screenWidth >= 600 && screenWidth <= 1000) {
      commonWidth = screenWidth * 0.7;
    } else if (screenWidth < 600) {
      commonWidth = screenWidth * 0.7;
    } else {
      commonWidth = screenWidth * 0.3;
    }

    bool isRow = screenWidth > 1000;

    return FadeIn(
      duration: const Duration(milliseconds: 1500),
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/d5.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 50),
          child: isRow
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildLeftCard(commonWidth),
                    _buildRightCard(commonWidth)
                  ],
                )
              : Column(
                  children: [
                    _buildLeftCard(commonWidth),
                    const SizedBox(height: 20),
                    _buildRightCard(commonWidth)
                  ],
                ),
        ),
      ),
    );
  }

  Widget _buildLeftCard(double width) {
    return SizedBox(
      width: width,
      child: FadeInLeft(
        child: Card(
          color: Colors.white.withOpacity(0.7),
          child: const Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  '¡Esperamos contar con su grata presencia en el 5to Congreso Internacional del Dolor - 3er Congreso Latinoamericano de Intervencionismo en Dolor (ALMID)!\n\n'
                  'Fecha: Jueves 26, Viernes 27 y Sábado 28 de Octubre.\n\n'
                  'Un evento de primer nivel con expertos en Medicina del Dolor, conferencias y talleres que sin duda enriquecerán nuestro conocimiento en esta área tan relevante.\n\n'
                  '¡Únase a esta experiencia única y enriquecedora!',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  'Cordialmente,',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  'Dr. Marco A. Narváez Tamayo',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
                Text(
                  'Presidente Comité Organizador',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildRightCard(double width) {
    return SizedBox(
      width: width,
      child: FadeInRight(
        child: Card(
          child: Image.asset(
            'assets/images/afiche001.jpg',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
