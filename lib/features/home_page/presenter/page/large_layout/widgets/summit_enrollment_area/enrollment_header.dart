import 'package:flutter/material.dart';

class EnrollmentHeader extends StatelessWidget {
  const EnrollmentHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(30.0),
      child: Text(
        'Inscripciones al congreso',
        style: TextStyle(color: Colors.white, fontSize: 40),
      ),
    );
  }
}
