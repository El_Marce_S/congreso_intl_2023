import 'package:cid2023/features/home_page/presenter/bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EnrollmentForm extends StatefulWidget {
  @override
  _EnrollmentFormState createState() => _EnrollmentFormState();
}

class _EnrollmentFormState extends State<EnrollmentForm> {
  final _formKey = GlobalKey<FormState>();
  String nombre = '';
  String apellido = '';
  String email = '';
  String celular = '';
  String pais = 'Bolivia'; // Valor predeterminado

  final List<Map<String, String>> paises = [
    // Países angloparlantes
    {'name': 'Australia', 'code': '+61'},
    {'name': 'Canada', 'code': '+1'},
    {'name': 'Ireland', 'code': '+353'},
    {'name': 'New Zealand', 'code': '+64'},
    {'name': 'South Africa', 'code': '+27'},
    {'name': 'United Kingdom', 'code': '+44'},
    {'name': 'United States', 'code': '+1'},

    // Países de América del Norte
    {'name': 'Mexico', 'code': '+52'},

    // Países de América del Sur
    {'name': 'Argentina', 'code': '+54'},
    {'name': 'Bolivia', 'code': '+591'},
    {'name': 'Brazil', 'code': '+55'},
    {'name': 'Chile', 'code': '+56'},
    {'name': 'Colombia', 'code': '+57'},
    {'name': 'Ecuador', 'code': '+593'},
    {'name': 'Guyana', 'code': '+592'},
    {'name': 'Paraguay', 'code': '+595'},
    {'name': 'Peru', 'code': '+51'},
    {'name': 'Suriname', 'code': '+597'},
    {'name': 'Uruguay', 'code': '+598'},
    {'name': 'Venezuela', 'code': '+58'},

    // Países de América Central y Caribe
    {'name': 'Belize', 'code': '+501'},
    {'name': 'Costa Rica', 'code': '+506'},
    {'name': 'El Salvador', 'code': '+503'},
    {'name': 'Guatemala', 'code': '+502'},
    {'name': 'Honduras', 'code': '+504'},
    {'name': 'Nicaragua', 'code': '+505'},
    {'name': 'Panama', 'code': '+507'},
    {'name': 'Antigua and Barbuda', 'code': '+1-268'},
    {'name': 'Bahamas', 'code': '+1-242'},
    {'name': 'Barbados', 'code': '+1-246'},
    {'name': 'Dominica', 'code': '+1-767'},
    {'name': 'Dominican Republic', 'code': '+1-809'},
    {'name': 'Grenada', 'code': '+1-473'},
    {'name': 'Haiti', 'code': '+509'},
    {'name': 'Jamaica', 'code': '+1-876'},
    {'name': 'Saint Kitts and Nevis', 'code': '+1-869'},
    {'name': 'Saint Lucia', 'code': '+1-758'},
    {'name': 'Saint Vincent and the Grenadines', 'code': '+1-784'},
    {'name': 'Trinidad and Tobago', 'code': '+1-868'},
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(60.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              double maxWidth = constraints.maxWidth * 0.5;
              return ConstrainedBox(
                constraints: BoxConstraints(maxWidth: maxWidth),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextFormField(
                        decoration: const InputDecoration(labelText: 'Nombre:'),
                        validator: (value) =>
                            value!.isEmpty ? 'Campo obligatorio' : null,
                        onSaved: (value) => nombre = value!,
                      ),
                      TextFormField(
                        decoration:
                            const InputDecoration(labelText: 'Apellido:'),
                        validator: (value) =>
                            value!.isEmpty ? 'Campo obligatorio' : null,
                        onSaved: (value) => apellido = value!,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(labelText: 'Email:'),
                        validator: (value) =>
                            !value!.contains('@') ? 'Email inválido' : null,
                        onSaved: (value) => email = value!,
                      ),
                      DropdownButtonFormField<String>(
                        decoration: const InputDecoration(labelText: 'País:'),
                        value: pais,
                        items: paises.map((Map<String, String> country) {
                          return DropdownMenuItem<String>(
                            value: country['name'],
                            child:
                                Text('${country['name']} (${country['code']})'),
                          );
                        }).toList(),
                        onChanged: (value) => setState(() => pais = value!),
                      ),
                      TextFormField(
                        decoration:
                            const InputDecoration(labelText: 'Celular:'),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value!.isEmpty) return 'Campo obligatorio';
                          if (int.tryParse(value) == null)
                            return 'Solo números';
                          return null;
                        },
                        onSaved: (value) => celular = value!,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState!
                                .save(); // Guarda los valores antes de enviar el evento
                            context.read<HomeBloc>().add(
                                  SummitEnrollEvent(
                                    name: nombre,
                                    lastName: apellido,
                                    email: email,
                                    country: pais,
                                    phone: celular,
                                  ),
                                );
                          }
                        },
                        child: const Text('Inscribirse'),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
