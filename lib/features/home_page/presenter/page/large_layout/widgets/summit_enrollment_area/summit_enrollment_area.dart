import 'package:cid2023/features/home_page/presenter/page/large_layout/promo_count_down.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/summit_enrollment_area/enrollment_card.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/summit_enrollment_area/enrollment_header.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class EnrollmentInfo {
  final double discountPercentage;
  final double regularPrice;
  final String title;
  final double finalPrice;
  final String discountInfo;
  final String description;
  final String enrollmentType;

  EnrollmentInfo({
    required this.discountPercentage,
    required this.regularPrice,
    required this.title,
    required this.finalPrice,
    required this.discountInfo,
    required this.description,
    required this.enrollmentType,
  });
}

List<EnrollmentInfo> enrollments = [
  EnrollmentInfo(
      discountPercentage: 15.625,
      regularPrice: 320,
      title: "Médicos especialistas",
      finalPrice: 270,
      discountInfo:
          "Descuento del ${15.625.toStringAsFixed(0)}% hasta el 15 de septiembre. Después de esta fecha, aplicarán precios regulares.",
      description: "Aplica para especialistas en la rama médica.",
      enrollmentType: 'Médico Especialista'),
  EnrollmentInfo(
      discountPercentage: 22.7273,
      regularPrice: 220,
      title: 'Médicos generales y profesionales en salud',
      finalPrice: 170,
      discountInfo:
          "Descuento del ${22.7273.toStringAsFixed(0)}% hasta el 15 de septiembre. Después de esta fecha, aplicarán precios regulares.",
      description:
          "Aplica para médicos generales y otros profesionales en salud.",
      enrollmentType: 'Médico General o Profesional en Salud'),
  EnrollmentInfo(
      discountPercentage: 20.0,
      regularPrice: 150,
      title: 'Residentes y estudiantes',
      finalPrice: 120,
      discountInfo:
          "Descuento del ${20.0.toStringAsFixed(0)}% hasta el 15 de septiembre. Después de esta fecha, aplicarán precios regulares.",
      description: "Aplica para residentes y estudiantes en formación médica.",
      enrollmentType: 'Residente o Estudiante'),
  EnrollmentInfo(
      discountPercentage: 20.0,
      regularPrice: 250,
      title: 'Socios ABD (Especialistas)',
      finalPrice: 200,
      discountInfo:
          "Descuento del ${20.0.toStringAsFixed(0)}% hasta el 15 de septiembre. Después de esta fecha, aplicarán precios regulares.",
      description: "Aplica para socios especializados de ABD.",
      enrollmentType: 'Socio Especialista'),
  EnrollmentInfo(
      discountPercentage: 25.0,
      regularPrice: 200,
      title: 'Socios ABD (M. Generales y otros profesionales en salud)',
      finalPrice: 150,
      discountInfo:
          "Descuento del ${25.0.toStringAsFixed(0)}% hasta el 15 de septiembre. Después de esta fecha, aplicarán precios regulares.",
      description:
          "Aplica para socios generales y otros profesionales en salud de ABD.",
      enrollmentType: 'Socio Profesional en Salud o Médico General'),
];

class EnrollmentArea extends StatelessWidget {
  const EnrollmentArea({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: HexColor('1d5a93'),
      child: Column(
        children: [
          const EnrollmentHeader(),
          const PromoCountDown(),
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              int numberOfCards;

              if (constraints.maxWidth > 1200) {
                numberOfCards = 5;
              } else if (constraints.maxWidth > 900) {
                numberOfCards = 5;
              } else if (constraints.maxWidth > 600) {
                numberOfCards = 5;
              } else if (constraints.maxWidth > 400) {
                numberOfCards = 5;
              } else {
                numberOfCards = 5;
              }

              int cardsToGenerate = (numberOfCards > enrollments.length)
                  ? enrollments.length
                  : numberOfCards;

              return Wrap(
                runSpacing: 20,
                alignment: WrapAlignment.center,
                children: List.generate(
                  cardsToGenerate,
                  (index) => EnrollmentCard(
                      discountPercentage: enrollments[index].discountPercentage,
                      regularPrice: enrollments[index].regularPrice,
                      finalPrice: enrollments[index].finalPrice,
                      title: enrollments[index].title,
                      discountInfo: enrollments[index].discountInfo,
                      description: enrollments[index].description,
                      enrollmentType: enrollments[index].enrollmentType,
                      shouldApplyDiscount: true),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
