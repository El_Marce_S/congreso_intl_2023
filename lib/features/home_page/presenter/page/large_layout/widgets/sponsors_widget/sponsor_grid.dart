import 'package:cid2023/features/home_page/data/models/sponsor_model.dart';
import 'package:flutter/material.dart';
import 'package:visibility_detector/visibility_detector.dart';

class SponsorsGrid extends StatelessWidget {
  const SponsorsGrid({
    Key? key,
    required this.sponsorLabel,
    required this.bgColor,
    required this.sponsors,
  }) : super(key: key);

  final String sponsorLabel;
  final String bgColor;
  final List<SponsorModel> sponsors;

  BoxDecoration getCardDecoration() {
    switch (bgColor) {
      case 'platinum':
        return BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          // Usar AssetImage o NetworkImage según lo necesites
          image: DecorationImage(
            image: AssetImage("assets/images/platinum.png"),
            fit: BoxFit.cover,
          ),
        );
      case 'gold':
        return BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          image: DecorationImage(
            image: AssetImage("assets/images/gold.png"),
            fit: BoxFit.cover,
          ),
        );
      case 'silver':
        return BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          image: DecorationImage(
            image: AssetImage("assets/images/silver.png"),
            fit: BoxFit.cover,
          ),
        );
      default:
        return BoxDecoration(color: Colors.grey);
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    int crossAxisCount;
    if (screenWidth < 600) {
      crossAxisCount = 1;
    } else if (screenWidth < 1024) {
      crossAxisCount = 2;
    } else {
      crossAxisCount = 4;
    }

    var itemWidth = screenWidth / crossAxisCount;
    var itemHeight = itemWidth * 0.6;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              sponsorLabel,
              style: TextStyle(
                fontSize: 50,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                decorationColor: Colors.black54,
                decorationThickness: 2.0,
              ),
            ),
          ),
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Wrap(
                direction: Axis.horizontal,
                spacing: 10.0,
                runSpacing: 10.0,
                children: List<Widget>.generate(
                  sponsors.length,
                  (int index) {
                    final sponsor = sponsors[index];
                    return VisibilityDetector(
                      key: Key('sponsorCard$index'),
                      onVisibilityChanged: (VisibilityInfo info) {
                        var visiblePercentage = info.visibleFraction * 100;
                      },
                      child: Container(
                        width: itemWidth,
                        height: itemHeight,
                        decoration: getCardDecoration(),
                        child: Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.all(40.0),
                            child: Image.network(sponsor.sponsorLogo),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
