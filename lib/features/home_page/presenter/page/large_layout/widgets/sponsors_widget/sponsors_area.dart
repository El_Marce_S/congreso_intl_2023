import 'package:cid2023/features/home_page/data/api/home_api.dart';
import 'package:cid2023/features/home_page/data/models/sponsor_model.dart';
import 'package:cid2023/features/home_page/presenter/bloc/home_bloc.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/sponsors_widget/sponsor_grid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SponsorsArea extends StatelessWidget {
  const SponsorsArea({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      create: (context) => HomeBloc(
        homeRepository: HomeApi(),
      )..add(
          const GetSponsorsEvent(),
        ),
      child: const _Page(),
    );
  }
}

class _Page extends StatelessWidget {
  const _Page({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {},
      child: const _Body(),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          BlocBuilder<HomeBloc, HomeState>(
            builder: (context, state) {
              if (state is LoadedSponsorsState) {
                final List<SponsorModel> platinumSponsors = state.sponsorList
                    .where((sponsor) => sponsor.sponsorLevel == 'Platinum')
                    .toList();
                final List<SponsorModel> goldSponsors = state.sponsorList
                    .where((sponsor) => sponsor.sponsorLevel == 'Gold')
                    .toList();
                final List<SponsorModel> silverSponsors = state.sponsorList
                    .where((sponsor) => sponsor.sponsorLevel == 'Silver')
                    .toList();

                return Column(
                  children: [
                    SponsorsGrid(
                      sponsorLabel: 'Auspiciadores Platinum',
                      bgColor: 'platinum',
                      sponsors: platinumSponsors, // Pasamos la lista filtrada
                    ),
                    SponsorsGrid(
                      sponsorLabel: 'Auspiciadores Oro',
                      bgColor: 'gold',
                      sponsors: goldSponsors, // Pasamos la lista filtrada
                    ),
                    SponsorsGrid(
                      sponsorLabel: 'Auspiciadores Plata',
                      bgColor: 'silver',
                      sponsors: silverSponsors, // Pasamos la lista filtrada
                    ),
                  ],
                );
              }
              return const CircularProgressIndicator();
            },
          ),
        ],
      ),
    );
  }
}
