import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SingleWorkshopCard extends StatelessWidget {
  const SingleWorkshopCard({Key? key, required this.index}) : super(key: key);

  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 6,
                child: CachedNetworkImage(
                  imageUrl:
                      "https://fastly.picsum.photos/id/630/200/200.jpg?hmac=X7UBUxsi2YahTLmW0-zKMMPOALeDjY5wPZGTPaGbDhU",
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              const Expanded(
                flex: 4,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Nombre del Taller'),
                      Text(
                        'Descripción de taller. Lorem Ipsum de dos a tres líneas.',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                      Text('Profesor A'),
                      Text('Profesor B'),
                      Text('Plazas ocupadas 0/10'),
                      Text('Fecha'),
                      Text('Hora'),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            color: Colors.blue,
            child: const Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'INSCRÍBETE',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
