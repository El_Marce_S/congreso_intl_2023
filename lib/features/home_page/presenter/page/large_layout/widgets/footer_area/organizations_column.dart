import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class OrganizationsColumn extends StatelessWidget {
  const OrganizationsColumn({super.key});

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'No se pudo lanzar $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: Text(
            'Organizaciones',
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
        ),
        ...List.generate(5, (index) {
          String text = index.isEven ? 'ALMID' : 'FEDELAT';
          return InkWell(
            onTap: () => _launchURL('https://www.google.com'),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                text,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
          );
        }),
      ],
    );
  }
}
