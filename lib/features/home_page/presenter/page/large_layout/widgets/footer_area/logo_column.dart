import 'package:flutter/material.dart';

class LogoColumn extends StatelessWidget {
  const LogoColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset('assets/icons/logo_cid2023.png'),
      ],
    );
  }
}
