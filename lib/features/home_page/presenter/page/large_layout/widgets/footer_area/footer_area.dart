import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/footer_area/contact_column.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/footer_area/logo_column.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/footer_area/organizations_column.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class FooterArea extends StatelessWidget {
  const FooterArea({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: HexColor('01457e'),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 40,
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxWidth > 700) {
              // Pantalla más ancha: Uso Row
              return const Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  LogoColumn(),
                  OrganizationsColumn(),
                  ContactUsColumn(),
                ],
              );
            } else {
              // Pantalla más estrecha: Uso Column
              return const Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  LogoColumn(),
                  SizedBox(height: 20), // Espaciado entre los widgets
                  OrganizationsColumn(),
                  SizedBox(height: 20), // Espaciado entre los widgets
                  ContactUsColumn(),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
