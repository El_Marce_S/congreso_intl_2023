import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUsColumn extends StatelessWidget {
  const ContactUsColumn({Key? key}) : super(key: key);

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'No se pudo lanzar $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: Text(
            'Contáctenos',
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
        ),
        InkWell(
          onTap: () => _launchURL('https://www.google.com'),
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Icon(Icons.email, color: Colors.white),
                Text(' info@congreso-dolor-2023-bolivia.org',
                    style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => _launchURL('https://www.google.com'),
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Icon(Icons.phone, color: Colors.white),
                Text(' 591- 62268518', style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => _launchURL('https://www.google.com'),
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Icon(Icons.facebook, color: Colors.white),
                Text(' fb.me/cid2023', style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        // Agregue los dos campos adicionales aquí
        InkWell(
          onTap: () => _launchURL('https://www.google.com'),
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Icon(Icons.link, color: Colors.white),
                Text(
                  ' Campo adicional 1',
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => _launchURL('https://www.google.com'),
          child: const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Icon(Icons.link, color: Colors.white),
                Text(
                  ' Campo adicional 2',
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
