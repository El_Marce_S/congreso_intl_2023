import 'package:cid2023/features/home_page/presenter/bloc/home_bloc.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/speakers_widget/speakers_grid.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/speakers_widget/speakers_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SpeakersArea extends StatelessWidget {
  const SpeakersArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return const _Page();
  }
}

class _Page extends StatelessWidget {
  const _Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is ErrorState) {
            (state.message);
        }
      },
      child: const _Body(),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SpeakersHeader(),
        BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is GettingDoctorsState) {
              return const SpinKitChasingDots(
                color: Colors.blue,
              );
            }

            if (state is LoadedSpeakers) {

              final nationals = state.doctorList
                  .where((doctor) => doctor.country == 'Bolivia')
                  .toList();
              final foreign = state.doctorList
                  .where((doctor) => doctor.country != 'Bolivia')
                  .toList();

              return Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Text(
                      'Expositores Internacionales',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SpeakersGrid(speakersList: foreign),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Text(
                      'Expositores Nacionales',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SpeakersGrid(speakersList: nationals),
                  ),
                ],
              );
            }

            return const SizedBox.shrink();
          },
        ),
      ],
    );
  }
}
