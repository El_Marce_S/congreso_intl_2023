import 'package:cid2023/features/home_page/data/models/doctor_model.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/speakers_widget/speaker_card.dart';
import 'package:flutter/material.dart';

class SpeakersGrid extends StatelessWidget {
  const SpeakersGrid({Key? key, required this.speakersList}) : super(key: key);

  final List<DoctorModel> speakersList;

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    var crossAxisCount;
    if (screenWidth < 600) {
      crossAxisCount = 1;
    } else if (screenWidth < 1200) {
      crossAxisCount = 2;
    } else {
      crossAxisCount = 4;
    }

    var itemWidth = screenWidth / crossAxisCount;

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Wrap(
          direction: Axis.horizontal,
          spacing: 10.0,
          runSpacing: 10.0,
          children: speakersList.map(
            (speaker) {
              return SizedBox(
                width: itemWidth,
                child: SingleSpeakerCard(speaker: speaker),
              );
            },
          ).toList(),
        );
      },
    );
  }
}
