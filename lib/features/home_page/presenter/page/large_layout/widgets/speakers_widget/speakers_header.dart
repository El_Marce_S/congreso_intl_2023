import 'package:flutter/material.dart';

class SpeakersHeader extends StatelessWidget {
  const SpeakersHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Conferencistas',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Conozca mas sobre nuestros conferencistas invitados',
              style: TextStyle(fontSize: 30),
            ),
          )
        ],
      ),
    );
  }
}
