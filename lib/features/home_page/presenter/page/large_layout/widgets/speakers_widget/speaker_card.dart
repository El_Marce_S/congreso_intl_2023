import 'package:cached_network_image/cached_network_image.dart';
import 'package:cid2023/core/countries.dart';
import 'package:cid2023/features/home_page/data/models/doctor_model.dart';
import 'package:circle_flags/circle_flags.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hexcolor/hexcolor.dart';

class SingleSpeakerCard extends StatelessWidget {
  const SingleSpeakerCard({
    Key? key,
    required this.speaker,
  }) : super(key: key);

  final DoctorModel speaker;

  @override
  Widget build(BuildContext context) {
      (speaker.imageUrl);
    return InkWell(
      onTap: () {
        // context.goNamed('speaker-profile',
        //     pathParameters: {'speakerId': speaker.uuid});
          ('Mandamos a una Nueva Pagina');
      },
      child: Container(
        width: 350, // Esto define el ancho fijo de la tarjeta
        child: Card(
          elevation: 20,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 158,
                height: 200,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                  ),
                  child: Container(
                    color: HexColor('2E4057'),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: speaker.imageUrl,
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${speaker.name} ${speaker.lastName}',
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(speaker.position),
                      Row(
                        children: [
                          Text(speaker.country),
                          const SizedBox(width: 5),
                          CircleFlag(
                            size: 30,
                            getCountryCode(speaker.country),
                          ),
                        ],
                      ),
                      Text(
                        speaker.summary,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
