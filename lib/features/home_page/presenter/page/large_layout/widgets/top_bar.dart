import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/single_top_button.dart';
import 'package:flutter/material.dart';

class TopBar extends StatelessWidget {
  final List<VoidCallback> onButtonPressedList;

  const TopBar({required this.onButtonPressedList, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Wrap(
        children: [
          SingleTopButton(
            title: 'Home',
            onPressed: () {
                ('Tapped Home');
              onButtonPressedList[0]();
            },
          ),
          SingleTopButton(
            title: 'Invitacion',
            onPressed: () {
                ('Tapped Invitacion');
              onButtonPressedList[1]();
            },
          ),
          SingleTopButton(
            title: 'Speakers',
            onPressed: () {
                ('Tapped Speakers');
              onButtonPressedList[2]();
            },
          ),
          SingleTopButton(
            title: 'Sponsors',
            onPressed: () {
                ('Tapped Sponsors');
              onButtonPressedList[3]();
            },
          ),
          SingleTopButton(
            title: 'Inscripciones al Congreso',
            onPressed: () {
                ('Tapped Inscripciones al Congreso');
              onButtonPressedList[4]();
            },
          ),
          SingleTopButton(
            title: 'Talleres e Incripciones',
            onPressed: () {
                ('Tapped Talleres e Incripciones');
              onButtonPressedList[5]();
            },
          ),
          SingleTopButton(
            title: 'Consultas',
            onPressed: () {
                ('Tapped Consultas');
              onButtonPressedList[6]();
            },
          ),
        ],
      ),
    );
  }
}
