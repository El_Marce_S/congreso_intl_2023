import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/footer_area/footer_area.dart';
import 'package:cid2023/features/home_page/presenter/page/large_layout/widgets/form_widget.dart';
import 'package:flutter/material.dart';

class ContactForm extends StatelessWidget {
  const ContactForm({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'Formulario de consultas',
          style: TextStyle(
            fontSize: 60,
          ),
        ),
        FormWidget(),
        FooterArea(),
      ],
    );
  }
}
