import 'package:cid2023/features/home_page/data/api/home_api.dart';
import 'package:cid2023/features/home_page/presenter/bloc/home_bloc.dart';
import 'package:cid2023/shared/error_dialog.dart';
import 'package:cid2023/shared/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LargeLayoutHome extends StatelessWidget {
  const LargeLayoutHome({Key? key, required this.contentWidget}) : super(key: key);

  final Widget contentWidget;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      create: (context) => HomeBloc(
        homeRepository: HomeApi(),
      )..add(const GetSpeakerDoctorsEvent()),
      child: _Page(
        contentWidget: contentWidget,
      ),
    );
  }
}

class _Page extends StatelessWidget {
  const _Page({
    Key? key,
    required this.contentWidget,
  }) : super(key: key);

  final Widget contentWidget;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is ErrorState) {
            ErrorDialog.showE(context);
            return;
          }
          if (state is LoadingState) {
            LoadingDialog(context);
            return;
          }
        },
        child: Column(
          children: [
            _Body(
              contentWidget: contentWidget,
            ),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    Key? key,
    required this.contentWidget,
  }) : super(key: key);

  final Widget contentWidget;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: contentWidget,
    );
  }
}
