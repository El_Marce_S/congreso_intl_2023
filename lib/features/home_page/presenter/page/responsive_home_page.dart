import 'package:cid2023/features/home_page/presenter/page/large_layout/desktop_page.dart';
import 'package:flutter/material.dart';

class ResponsiveHomePage extends StatelessWidget {
  const ResponsiveHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DesktopView();
  }
}
