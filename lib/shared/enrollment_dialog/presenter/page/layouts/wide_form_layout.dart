import 'package:flutter/material.dart';

class WideFormLayout extends StatelessWidget {
  const WideFormLayout({
    super.key,
    required TextEditingController nameController,
    required TextEditingController lastNameController,
    required TextEditingController emailController,
    required TextEditingController emailVerificationController,
    required TextEditingController countryController,
    required TextEditingController phoneController,
    required TextEditingController nitController,
    required TextEditingController nameToNitController,
  })  : _nameController = nameController,
        _lastNameController = lastNameController,
        _emailController = emailController,
        _emailVerificationController = emailVerificationController,
        _countryController = countryController,
        _phoneController = phoneController,
        _nitController = nitController,
        _nameToNitController = nameToNitController;

  final TextEditingController _nameController;
  final TextEditingController _lastNameController;
  final TextEditingController _emailController;
  final TextEditingController _emailVerificationController;
  final TextEditingController _countryController;
  final TextEditingController _phoneController;
  final TextEditingController _nitController;
  final TextEditingController _nameToNitController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: _nameController,
                decoration: const InputDecoration(labelText: 'Nombre'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: TextFormField(
                controller: _lastNameController,
                decoration: const InputDecoration(labelText: 'Apellido'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(labelText: 'Email'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  } else if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      .hasMatch(value)) {
                    return 'Por favor, introduce un email válido';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: TextFormField(
                controller: _emailVerificationController,
                decoration:
                    const InputDecoration(labelText: 'Email verificación'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  } else if (value != _emailController.text) {
                    return 'Los emails no coinciden';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: _countryController,
                decoration: const InputDecoration(labelText: 'Pais'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: TextFormField(
                controller: _phoneController,
                decoration: const InputDecoration(labelText: 'Celular'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  } else if (!RegExp(r"^[0-9]+$").hasMatch(value)) {
                    return 'Por favor, introduce un número válido';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: _nitController,
                decoration: const InputDecoration(
                    labelText: 'NIT (Para extranjeros, ingresar DNI)'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: TextFormField(
                controller: _nameToNitController,
                decoration: const InputDecoration(
                    labelText:
                        'Nombre en factura o apellido para el documento'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Este campo es obligatorio';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
