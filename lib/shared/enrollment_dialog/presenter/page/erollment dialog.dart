import 'dart:html' as html;

import 'package:cid2023/shared/custom_success_dialog.dart';
import 'package:cid2023/shared/enrollment_dialog/data/api/enroll_api.dart';
import 'package:cid2023/shared/enrollment_dialog/data/models/new_enrollment_request_model.dart';
import 'package:cid2023/shared/enrollment_dialog/presenter/bloc/enroll_dialog_bloc.dart';
import 'package:cid2023/shared/enrollment_dialog/presenter/page/layouts/small_form_layout.dart';
import 'package:cid2023/shared/enrollment_dialog/presenter/page/layouts/wide_form_layout.dart';
import 'package:cid2023/shared/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_browser_detect/web_browser_detect.dart';

class EnrollmentDialog extends StatelessWidget {
  final String enrollmentType;
  final double discountPercentage;
  final double regularPrice;
  final double finalPrice;

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _emailVerificationController =
      TextEditingController();
  final TextEditingController _countryController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _nitController = TextEditingController();
  final TextEditingController _nameToNitController = TextEditingController();
  final TextEditingController _typeController = TextEditingController();

  EnrollmentDialog({
    Key? key,
    required this.enrollmentType,
    required this.discountPercentage,
    required this.regularPrice,
    required this.finalPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EnrollDialogBloc>(
      create: (context) => EnrollDialogBloc(
        enrollRepository: EnrollApi(),
      ),
      child: _Page(
          formKey: _formKey,
          enrollmentType: enrollmentType,
          finalPrice: finalPrice,
          nameController: _nameController,
          lastNameController: _lastNameController,
          emailController: _emailController,
          emailVerificationController: _emailVerificationController,
          countryController: _countryController,
          phoneController: _phoneController,
          nitController: _nitController,
          nameToNitController: _nameToNitController,
          typeController: _typeController,
          discountPercentage: discountPercentage,
          regularPrice: regularPrice),
    );
  }

  void show(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return this;
      },
    );
  }
}

class _Page extends StatelessWidget {
  _Page({
    required GlobalKey<FormState> formKey,
    required this.enrollmentType,
    required this.finalPrice,
    required TextEditingController nameController,
    required TextEditingController lastNameController,
    required TextEditingController emailController,
    required TextEditingController emailVerificationController,
    required TextEditingController countryController,
    required TextEditingController phoneController,
    required TextEditingController nitController,
    required TextEditingController nameToNitController,
    required TextEditingController typeController,
    required this.discountPercentage,
    required this.regularPrice,
  })  : _formKey = formKey,
        _nameController = nameController,
        _lastNameController = lastNameController,
        _emailController = emailController,
        _emailVerificationController = emailVerificationController,
        _countryController = countryController,
        _phoneController = phoneController,
        _nitController = nitController,
        _nameToNitController = nameToNitController,
        _typeController = typeController;

  final GlobalKey<FormState> _formKey;
  final String enrollmentType;
  final double finalPrice;
  final TextEditingController _nameController;
  final TextEditingController _lastNameController;
  final TextEditingController _emailController;
  final TextEditingController _emailVerificationController;
  final TextEditingController _countryController;
  final TextEditingController _phoneController;
  final TextEditingController _nitController;
  final TextEditingController _nameToNitController;
  final TextEditingController _typeController;
  final double discountPercentage;
  final double regularPrice;

  final browser = Browser();

  @override
  Widget build(BuildContext context) {
    bool isSafari = (browser.browser == 'Safari');
    return BlocListener<EnrollDialogBloc, EnrollDialogState>(
      listener: (context, state) {
        if (state is LoadingState) {
          LoadingDialog(context);
          return;
        }
        if (state is GotPaymentMethodLinkState) {
          Navigator.of(context).pop();
          _handleConfirmationDialog(
            context,
            isSafari: isSafari,
            state: state,
            email: _emailController.text,
          );
          return;
        }
        if (state is PaymentConfirmedState) {
          Navigator.of(context).pop();
          CustomSuccessDialog(
            title: 'Pago Procesado con exito',
            contentText:
                'El pago ha sido validado a través de la pasarela de pago. Por favor, revisa tu correo electrónico, tanto en la bandeja de entrada como en la carpeta de Spam. Deberías haber recibido un mensaje con un código QR y un código de 12 caracteres. Ambos son esenciales. Con este código, podrás inscribirte a los talleres del congreso. Además, el código QR servirá para acreditarte y permitir tu ingreso al congreso.',
            onSuccess: () {
              Navigator.of(context).pop();
            },
          ).showSuccessDialog(context);
          return;
        }
      },
      child: _Body(
        formKey: _formKey,
        enrollmentType: enrollmentType,
        finalPrice: finalPrice,
        nameController: _nameController,
        lastNameController: _lastNameController,
        emailController: _emailController,
        emailVerificationController: _emailVerificationController,
        countryController: _countryController,
        phoneController: _phoneController,
        nitController: _nitController,
        nameToNitController: _nameToNitController,
        typeController: _typeController,
        isSafari: isSafari,
        discountPercentage: discountPercentage,
        regularPrice: regularPrice,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required GlobalKey<FormState> formKey,
    required this.enrollmentType,
    required this.finalPrice,
    required TextEditingController nameController,
    required TextEditingController lastNameController,
    required TextEditingController emailController,
    required TextEditingController emailVerificationController,
    required TextEditingController countryController,
    required TextEditingController phoneController,
    required TextEditingController nitController,
    required TextEditingController nameToNitController,
    required TextEditingController typeController,
    required this.discountPercentage,
    required this.regularPrice,
    required this.isSafari,
  })  : _formKey = formKey,
        _nameController = nameController,
        _lastNameController = lastNameController,
        _emailController = emailController,
        _emailVerificationController = emailVerificationController,
        _countryController = countryController,
        _phoneController = phoneController,
        _nitController = nitController,
        _nameToNitController = nameToNitController,
        _typeController = typeController;

  final GlobalKey<FormState> _formKey;
  final String enrollmentType;
  final double finalPrice;
  final TextEditingController _nameController;
  final TextEditingController _lastNameController;
  final TextEditingController _emailController;
  final TextEditingController _emailVerificationController;
  final TextEditingController _countryController;
  final TextEditingController _phoneController;
  final TextEditingController _nitController;
  final TextEditingController _nameToNitController;
  final TextEditingController _typeController;
  final double discountPercentage;
  final double regularPrice;
  final bool isSafari;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Ingrese sus datos para proceder con su registro.',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Text(
                'Usted se está registrando al Congreso Internacional del Dolor '
                '2023. Por ser $enrollmentType su tarifa es de \$${finalPrice.toStringAsFixed(2)}.',
              ),
              const SizedBox(height: 20),
              const Text(
                'Necesitamos algunos datos para continuar:',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.maxWidth > 600) {
                    return WideFormLayout(
                        nameController: _nameController,
                        lastNameController: _lastNameController,
                        emailController: _emailController,
                        emailVerificationController:
                            _emailVerificationController,
                        countryController: _countryController,
                        phoneController: _phoneController,
                        nitController: _nitController,
                        nameToNitController: _nameToNitController);
                  } else {
                    return SmallFormLayout(
                        nameController: _nameController,
                        lastNameController: _lastNameController,
                        emailController: _emailController,
                        emailVerificationController:
                            _emailVerificationController,
                        countryController: _countryController,
                        phoneController: _phoneController,
                        nitController: _nitController,
                        nameToNitController: _nameToNitController);
                  }
                },
              ),
              const SizedBox(height: 20),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    OutlinedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Atrás'),
                    ),
                    const SizedBox(width: 10),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          final newEnrollModel = NewEnrollModel(
                            enrolmentType: enrollmentType,
                            name: _nameController.text,
                            lastName: _lastNameController.text,
                            email: _emailController.text,
                            country: _countryController.text,
                            phone: _phoneController.text,
                            nit: _nitController.text,
                            nameToNit: _nameToNitController.text,
                            type: _typeController.text,
                            discount: discountPercentage,
                            regularPrice: regularPrice,
                            finalPrice: finalPrice,
                            isSafari: isSafari,
                          );
                          context.read<EnrollDialogBloc>().add(
                                RegisterNewEnrollEvent(
                                    newEnrollData: newEnrollModel),
                              );
                        }
                      },
                      child: const Text('Registrarse'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_processPayment(BuildContext context,
    {required String debtLink,
    required String enrollmentUUID,
    required bool isSafari}) {
  if (isSafari) {
    Navigator.of(context).pop();
  } else {
    html.window.open(debtLink, '_blank');
    context.read<EnrollDialogBloc>().add(
          CheckPaymentEvent(enrollShortUuid: enrollmentUUID),
        );
  }
}

void _handleConfirmationDialog(
  BuildContext context, {
  required GotPaymentMethodLinkState state,
  required String email,
  required bool isSafari,
}) {
  if (state.debtLink == 'Sent To Email') {
    CustomSuccessDialog(
      title: 'Confirmación de Inscripción',
      contentText:
          'Hemos registrado exitosamente sus datos para la inscripción. Por favor, revise su correo ($email): hemos enviado un enlace para el pago que estará vigente durante 10 minutos. Si no lo encuentra, verifique su carpeta de spam.',
      onSuccess: () {
        Navigator.of(context).pop();
      },
    ).showSuccessDialog(context);
  } else {
    CustomSuccessDialog(
      title: 'Confirmación de Inscripción',
      contentText:
          'Hemos registrado con éxito tus datos para la inscripción. A continuación, serás dirigido a la pasarela de pagos para finalizar el proceso. Por favor, no cierres esta ventana.',
      onSuccess: () {
        _processPayment(
          context,
          isSafari: isSafari,
          debtLink: state.debtLink,
          enrollmentUUID: state.enrollmentUUID,
        );
      },
    ).showSuccessDialog(context);
  }
}
