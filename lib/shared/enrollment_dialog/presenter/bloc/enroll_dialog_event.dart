part of 'enroll_dialog_bloc.dart';

abstract class EnrollDialogEvent extends Equatable {
  const EnrollDialogEvent();
}

class RegisterNewEnrollEvent extends EnrollDialogEvent {
  const RegisterNewEnrollEvent({required this.newEnrollData});

  final NewEnrollModel newEnrollData;

  @override
  List<Object> get props => [newEnrollData];
}

class CheckPaymentEvent extends EnrollDialogEvent {
  const CheckPaymentEvent({required this.enrollShortUuid});

  final String enrollShortUuid;

  @override
  List<Object> get props => [enrollShortUuid];
}
