part of 'enroll_dialog_bloc.dart';

abstract class EnrollDialogState extends Equatable {
  const EnrollDialogState();
}

class EnrollDialogInitial extends EnrollDialogState {
  @override
  List<Object> get props => [];
}

class LoadingState extends EnrollDialogState {
  @override
  List<Object> get props => [];
}

class GotPaymentMethodLinkState extends EnrollDialogState {
  const GotPaymentMethodLinkState(
      {required this.debtLink, required this.enrollmentUUID});

  final String debtLink;
  final String enrollmentUUID;

  @override
  List<Object> get props => [debtLink, enrollmentUUID];
}

class ErrorState extends EnrollDialogState {
  const ErrorState({required this.message});

  final String message;

  @override
  List<Object> get props => [message];
}

class PaymentConfirmedState extends EnrollDialogState {
  const PaymentConfirmedState();

  @override
  List<Object> get props => [];
}
