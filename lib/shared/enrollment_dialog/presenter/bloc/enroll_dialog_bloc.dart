import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cid2023/shared/enrollment_dialog/data/models/new_enrollment_request_model.dart';
import 'package:cid2023/shared/enrollment_dialog/domain/repository/enroll_repository.dart';
import 'package:equatable/equatable.dart';

part 'enroll_dialog_event.dart';

part 'enroll_dialog_state.dart';

class EnrollDialogBloc extends Bloc<EnrollDialogEvent, EnrollDialogState> {
  Timer? _timer;

  EnrollDialogBloc({required this.enrollRepository})
      : super(EnrollDialogInitial()) {
    on<EnrollDialogEvent>((event, emit) {});
    on<RegisterNewEnrollEvent>(_onRegisterNewEnrollEvent);
    on<CheckPaymentEvent>(_onCheckPaymentEvent);
  }

  final EnrollRepository enrollRepository;

  Future<void> _onRegisterNewEnrollEvent(
      RegisterNewEnrollEvent event, Emitter<EnrollDialogState> emit) async {
    emit(
      LoadingState(),
    );

    final response = await enrollRepository.registerNewEnroll(
        newEnrollModel: event.newEnrollData);

    response.when(
      ok: (data) {
        final debtLink = data['debtUrl']!;
        final enrollmentUUID = data['enrollmentUUID']!;

        emit(
          GotPaymentMethodLinkState(
              debtLink: debtLink, enrollmentUUID: enrollmentUUID),
        );
      },
      err: (e) {
        emit(
          ErrorState(
            message: e.message,
          ),
        );
      },
    );
  }

  Completer<void>? _completer;

  Future<void> _onCheckPaymentEvent(
      CheckPaymentEvent event, Emitter<EnrollDialogState> emit) async {
    emit(LoadingState());

    _timer?.cancel();
    _completer = Completer();

    _timer = Timer.periodic(const Duration(seconds: 3), (timer) async {
        ("verificando pago");
      final response = await enrollRepository.checkPayment(
          enrollmentUUID: event.enrollShortUuid);

      response.when(ok: (isConfirmed) {
        if (isConfirmed) {
            ("Pago confirmado");
          timer.cancel();
          if (!_completer!.isCompleted) {
            emit(PaymentConfirmedState());
            _completer!.complete();
          }
        }
        // Si no se confirma, no hagas nada y sigue comprobando.
      }, err: (error) {
          ("Error al verificar el pago: ${error.message}");
        timer.cancel();
        if (!_completer!.isCompleted) {
          _completer!.completeError(error);
        }
      });
    });

    return _completer!.future;
  }

  @override
  Future<void> close() {
    _timer?.cancel();
    if (!_completer!.isCompleted) {
      _completer
          ?.complete(); // asegurarse de que se complete si no se ha hecho antes
    }
    return super.close();
  }
}
