import 'package:cid2023/shared/enrollment_dialog/domain/entities/new_enroll_entity.dart';

class NewEnrollModel extends NewEnrollEntity {
  NewEnrollModel({
    required String name,
    required bool isSafari,
    required String lastName,
    required String email,
    required String country,
    required String phone,
    required String nit,
    required String nameToNit,
    required String type,
    required double discount,
    required double regularPrice,
    required double finalPrice,
    required String enrolmentType,
  }) : super(
            name: name,
            isSafari: isSafari,
            lastName: lastName,
            email: email,
            country: country,
            phone: phone,
            nit: nit,
            nameToNit: nameToNit,
            type: type,
            discount: discount,
            regularPrice: regularPrice,
            finalPrice: finalPrice,
            enrollmentType: enrolmentType);

  factory NewEnrollModel.fromJson(Map<String, dynamic> json) {
    return NewEnrollModel(
      name: json['name'] ?? '',
      lastName: json['lastName'] ?? '',
      email: json['email'] ?? '',
      country: json['country'] ?? '',
      phone: json['phone'] ?? '',
      nit: json['nit'] ?? '',
      nameToNit: json['nameToNit'] ?? '',
      type: json['type'] ?? '',
      discount: json['discount'] ?? 0,
      regularPrice: json['regularPrice'] ?? 0,
      finalPrice: json['finalPrice'] ?? 0,
      enrolmentType: json['enrollmentType'] ?? 0,
      isSafari: json['isSafari'] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'isSafari': isSafari,
      'lastName': lastName,
      'email': email,
      'country': country,
      'phone': phone,
      'nit': nit,
      'nameToNit': nameToNit,
      'type': type,
      'discount': discount,
      'regularPrice': regularPrice,
      'finalPrice': finalPrice,
      'enrollmentType': enrollmentType
    };
  }
}
