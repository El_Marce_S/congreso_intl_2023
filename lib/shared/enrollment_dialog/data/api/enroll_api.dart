import 'dart:convert';

import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/core/services/http_helper/http_helper.dart';
import 'package:cid2023/shared/enrollment_dialog/data/models/new_enrollment_request_model.dart';
import 'package:cid2023/shared/enrollment_dialog/domain/repository/enroll_repository.dart';
import 'package:oxidized/src/result.dart';

class EnrollApi extends EnrollRepository {
  @override
  Future<Result<Map<String, String>, Failure>> registerNewEnroll(
      {required NewEnrollModel newEnrollModel}) async {
    final httpHelper = HttpHelper();
    try {
      const endpoint = '/enrollments/create-summit-enroll';
      final response = await httpHelper.post(endpoint, body: newEnrollModel);
      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final debtUrl = decodedData['debtUrl'] as String;
          final enrollmentUUID = decodedData['enrollmentUUID'] as String;

          return Result.ok(
              {'debtUrl': debtUrl, 'enrollmentUUID': enrollmentUUID});
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
        (e);
      return Result.err(ServerFailure(message: e.toString()));
    }
  }

  @override
  Future<Result<bool, Failure>> checkPayment(
      {required String enrollmentUUID}) async {
    final httpHelper = HttpHelper();

    try {
      final endpoint =
          '/enrollments/check-payment?enrollUUID=$enrollmentUUID';
      final response = await httpHelper.get(endpoint);

      return response.when(
        ok: (data) {
          final decodedData = json.decode(data);
          final bool confirmed = decodedData['confirmed'];
          return Result.ok(confirmed);
        },
        err: (errorMessage, _) {
          return Result.err(ServerFailure(message: errorMessage));
        },
      );
    } catch (e) {
      return Result.err(
        ServerFailure(
          message: e.toString(),
        ),
      );
    }
  }
}
