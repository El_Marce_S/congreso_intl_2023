import 'package:cid2023/core/models/failures_model.dart';
import 'package:cid2023/shared/enrollment_dialog/data/models/new_enrollment_request_model.dart';
import 'package:oxidized/oxidized.dart';

abstract class EnrollRepository {
  Future<Result<Map<String, String>, Failure>> registerNewEnroll(
      {required NewEnrollModel newEnrollModel});

  Future<Result<bool, Failure>> checkPayment({required String enrollmentUUID});
}
