class NewEnrollEntity {
  const NewEnrollEntity({
    required this.name,
    required this.lastName,
    required this.email,
    required this.country,
    required this.phone,
    required this.nit,
    required this.nameToNit,
    required this.type,
    required this.discount,
    required this.regularPrice,
    required this.finalPrice,
    required this.enrollmentType,
    required this.isSafari,
  });

  final String name;
  final String lastName;
  final bool isSafari;
  final String enrollmentType;
  final String email;
  final String country;
  final String phone;
  final String nit;
  final String nameToNit;
  final String type;
  final double discount;
  final double regularPrice;
  final double finalPrice;
}
