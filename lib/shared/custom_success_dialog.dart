import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class CustomSuccessDialog {
  final String? title;
  final String? contentText;

  CustomSuccessDialog({
    this.title,
    this.contentText,
    required this.onSuccess,
  });

  final Null Function() onSuccess;

  Future<dynamic> showSuccessDialog(BuildContext context) {
    return AwesomeDialog(
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: false,
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.rightSlide,
      headerAnimationLoop: false,
      title: title ?? 'Success',
      desc: contentText ?? 'Exito!',
      btnOkOnPress: onSuccess,
      btnOkIcon: Icons.check,
      btnOkColor: Colors.teal,
    ).show();
  }
}
