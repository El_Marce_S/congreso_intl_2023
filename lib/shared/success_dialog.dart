import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class SuccessDialog {
  static Future<dynamic> showSuccessDialog(BuildContext context) {
    return AwesomeDialog(
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: false,
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.rightSlide,
      headerAnimationLoop: false,
      title: 'Exito',
      desc: 'Gracias, alguien se pondra en contacto contigo pronto',
      btnOkOnPress: () {
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/home',
          (route) => false,
        );
      },
      btnOkIcon: Icons.check,
      btnOkColor: Colors.teal,
    ).show();
  }
}
