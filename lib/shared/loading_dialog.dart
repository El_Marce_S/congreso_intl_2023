import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingDialog {
  LoadingDialog(
    BuildContext context, {
    iconPath = 'assets/animations/loading.json',
  }) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) => WillPopScope(
        onWillPop: () async => false,
        child: Lottie.asset(iconPath),
      ),
    );
  }
}
